#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"

#define PRINT(msg) ({printf("    >> %s\n", msg); fflush(stdout);})
//#define WAIT(msg) ({printf("    >> %s, press any key\n", msg); getchar();})

//#define PRINT(msg) ({ })
//#define WAIT(msg) ({ })

void do_something_else(char *data) {
    if (strlen(data) < 9) {
        printf("Returning! strlen(data): %lu\n", strlen(data));
        fflush(stdout);
        return;
    }

    if (data[0] == 'R') {
        if (data[1] == 'E') {
            if (data[2] == 'S') {
                if (data[3] == 'M') {
                    if (data[4] == 'A') {
                        if (data[5] == 'C') {
                            if (data[6] == 'K') {
                                if (data[7] == '!') {
                                    if (data[8] == '!') {
                                        PRINT(">> CRASH!\n");
                                        char *blah = 0;
                                        blah[0] = 'A';
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

void do_something(char *data) {
    char buf1[0x10000];
    char buf2[0x10000];
    PRINT("Long prelude");
    for (int i = 0; i < 0x10000; i++) {
        memcpy(buf1, buf2, sizeof(buf1));
    }
    PRINT("Done with long prelude");

    do_something_else(data);
}

int main(int argc, char** argv) {
    if (argc != 2) {
        printf("USAGE: %s DATA\n", argv[0]);
        return 1;
    }

    PRINT("Triggering first watchpoint");
    asm("mov $0xf00dfeed, %%rcx;"
        "mov %0, %%rax;"
        "mov %1, %%rbx;"
        "int3;"
        "xor %%rcx, %%rcx;"
         :
         : "r" (argv[1]), "r" (strlen(argv[1]))
         : "rcx", "rbx", "rax"
    );

    PRINT("Doing something");
    do_something(argv[1]);

    asm("mov $0xfeedf00d, %%rcx;"
        "int3;"
        "xor %%rcx, %%rcx;"
         :
         :
         : "%rcx"
    );
}
