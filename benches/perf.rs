extern crate nix;
extern crate perfcnt;

use std::collections::HashMap;

use perfcnt::linux::{HardwareEventType, PerfCounterBuilderLinux};
use perfcnt::{AbstractPerfCounter, PerfCounter};

use std::fs::File;
use std::io::{Read, Seek, SeekFrom, Take, Write};
use std::os::unix::process::CommandExt;
use std::process::Command;

use libc::{sysconf, _SC_IOV_MAX, _SC_PAGESIZE};

use nix::errno;
use nix::sys::ptrace;
use nix::sys::signal::Signal;
use nix::sys::uio;
use nix::sys::wait::{waitpid, WaitStatus};
use nix::unistd::Pid;

#[macro_use]
#[path = "../src/log.rs"]
mod log;
#[path = "../src/snapshot.rs"]
mod snapshot;
#[path = "../src/utils.rs"]
mod utils;
use snapshot::MemRegion;

use criterion::{
    criterion_group, criterion_main, AxisScale, BenchmarkId, Criterion, PlotConfiguration,
};

fn do_work() {
    let mut data: HashMap<usize, Vec<usize>> = HashMap::new();
    for i in 0..0x1000usize {
        if i % 4 == 0 {
            data.entry(4).or_insert(Vec::new()).push(i);
        }
        if i % 3 == 0 {
            data.entry(3).or_insert(Vec::new()).push(i);
        }
        if i % 2 == 0 {
            data.entry(2).or_insert(Vec::new()).push(i);
        }
    }
}

pub fn measure_perf_counters(c: &mut Criterion) {
    let plot_config = PlotConfiguration::default().summary_scale(AxisScale::Logarithmic);

    let mut group = c.benchmark_group("perf");
    group.bench_function("branch_counter", |b| {
        b.iter(|| {
            let mut counter =
                PerfCounterBuilderLinux::from_hardware_event(HardwareEventType::BranchInstructions)
                    .for_pid(Pid::this().as_raw() as i32)
                    .inherit()
                    .disable()
                    .exclude_kernel()
                    .exclude_hv()
                    .exclude_idle()
                    .finish()
                    .expect("Could not create branch counter");
            counter.start().unwrap();
            do_work();
            counter.stop().unwrap();
        })
    });

    group.bench_function("instruction_counter", |b| {
        b.iter(|| {
            let mut counter =
                PerfCounterBuilderLinux::from_hardware_event(HardwareEventType::Instructions)
                    .for_pid(Pid::this().as_raw() as i32)
                    .inherit()
                    .disable()
                    .exclude_kernel()
                    .exclude_hv()
                    .exclude_idle()
                    .finish()
                    .expect("Could not create instruction counter");
            counter.start().unwrap();
            do_work();
            counter.stop().unwrap();
        })
    });

    group.bench_function("instruction_and_branch_counters", |b| {
        b.iter(|| {
            let mut branch_counter =
                PerfCounterBuilderLinux::from_hardware_event(HardwareEventType::BranchInstructions)
                    .for_pid(Pid::this().as_raw() as i32)
                    .inherit()
                    .disable()
                    .exclude_kernel()
                    .exclude_hv()
                    .exclude_idle()
                    .finish()
                    .expect("Could not create branch counter");
            let mut instruction_counter =
                PerfCounterBuilderLinux::from_hardware_event(HardwareEventType::Instructions)
                    .for_pid(Pid::this().as_raw() as i32)
                    .inherit()
                    .disable()
                    .exclude_kernel()
                    .exclude_hv()
                    .exclude_idle()
                    .finish()
                    .expect("Could not create instruction counter");
            branch_counter.start().unwrap();
            instruction_counter.start().unwrap();
            do_work();
            branch_counter.stop().unwrap();
            instruction_counter.stop().unwrap();
        })
    });

    group.bench_function("no_perf", |b| {
        b.iter(|| {
            do_work();
        })
    });

    group.finish();
}

criterion_group!(benches, measure_perf_counters);
criterion_main!(benches);
