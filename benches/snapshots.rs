extern crate nix;

use std::fs::File;
use std::io::{Read, Seek, SeekFrom, Take, Write};
use std::os::unix::process::CommandExt;
use std::process::Command;

use libc::{sysconf, _SC_IOV_MAX, _SC_PAGESIZE};

use nix::errno;
use nix::sys::ptrace;
use nix::sys::signal::Signal;
use nix::sys::uio;
use nix::sys::wait::{waitpid, WaitStatus};
use nix::unistd::Pid;

#[macro_use]
#[path = "../src/log.rs"]
mod log;
#[path = "../src/snapshot.rs"]
mod snapshot;
#[path = "../src/utils.rs"]
mod utils;
use snapshot::MemRegion;

use criterion::{
    criterion_group, criterion_main, AxisScale, BenchmarkId, Criterion, PlotConfiguration,
};

fn load_mem_regions(
    page_size: u64,
    pid: Pid,
    regions: &mut Vec<MemRegion>,
    mem_fd: &mut File,
    data: &mut Vec<u8>,
) {
    regions.clear();
    data.clear();

    let output = std::fs::read_to_string(&format!("/proc/{}/maps", pid)).unwrap();
    for (idx, line) in output.split("\n").enumerate() {
        //            0                1      2       3     4                             5
        // 558e65308000-558e6530a000 r--p 00000000 fd:01 36700321 /usr/bin/cat
        // 558e65308000-558e6530a000 r--p 00000000 fd:01 36700321 [stack]
        let parts: Vec<&str> = line.split_whitespace().collect();
        let perms = match parts.get(1) {
            Some(v) => v.as_bytes(),
            None => continue,
        };
        // rwxp
        if perms[1] != b'w' {
            continue;
        }

        let range_parts: Vec<&str> = parts[0].split("-").collect();
        let start: u64 =
            u64::from_str_radix(range_parts[0], 16).expect("Could not parse start range");
        let end: u64 = u64::from_str_radix(range_parts[1], 16).expect("Could not parse end range");
        regions.push(MemRegion::new(page_size, idx as u64, start, end));

        let mut region_data = vec![0; (end - start) as usize];
        mem_fd.seek(SeekFrom::Start(start)).expect("Could not seek");
        mem_fd
            .take(end - start)
            .read(&mut region_data)
            .expect("Could not copy process memory");
        data.extend(region_data);
    }
}

/// Restore `i` number of pages by restoring entire regions at a time.
///
/// Average region contains ~14 pages in my bash shell process.
///
/// if `sparse_page_per_region` is true, then only one page per region counts
/// towards the total page count. Otherwise, every page in the region counts
/// towards the page count.
fn restore_via_mem(
    i: usize,
    sparse_page_per_region: bool,
    mem_fd: &mut File,
    mem_regions: &Vec<MemRegion>,
    restore_data: &Vec<u8>,
) {
    let mut count: usize = 0;
    while count < i {
        let mut offset: usize = 0;
        for region in mem_regions.iter() {
            mem_fd
                .seek(SeekFrom::Start(region.start))
                .expect("Could not seek");
            mem_fd
                .write(&restore_data[offset..offset + region.size])
                .expect("Could not restore memory");
            if sparse_page_per_region {
                count += 1; //region.num_pages as usize;
            } else {
                // every page counts
                count += region.num_pages as usize;
            }
            offset += region.size;
        }
    }
}

/// Restore `i` number of pages by restoring entire regions at a time.
///
/// if `sparse_page_per_region` is true, then only one page per region counts
/// towards the total page count. Otherwise, every page in the region counts
/// towards the page count.
fn restore_region_via_process_vm_write(
    i: usize,
    sparse_page_per_region: bool,
    target_pid: Pid,
    mem_fd: &mut File,
    mem_regions: &Vec<MemRegion>,
    restore_data: &Vec<u8>,
) {
    let mut count: usize = 0;
    let mut local_iovecs: Vec<uio::IoVec<&[u8]>> = Vec::new();
    let mut remote_iovecs: Vec<uio::RemoteIoVec> = Vec::new();
    while count < i {
        let mut offset: usize = 0;
        for region in mem_regions.iter() {
            local_iovecs.push(uio::IoVec::from_slice(
                &restore_data[offset..offset + region.size],
            ));
            remote_iovecs.push(uio::RemoteIoVec {
                base: region.start as usize,
                len: region.size as usize,
            });
            if sparse_page_per_region {
                count += 1;
            } else {
                // every page counts
                count += region.num_pages as usize;
            }
            offset += region.size;
        }
    }

    uio::process_vm_writev(target_pid, &local_iovecs, &remote_iovecs)
        .expect("Could not reset child process");
}

/// Restore `i` number of pages by restoring entire regions at a time.
///
/// if `sparse_page_per_region` is true, then only one page per region counts
/// towards the total page count. Otherwise, every page in the region counts
/// towards the page count.
fn restore_region_via_process_vm_write_with_page_size(
    i: usize,
    sparse_page_per_region: bool,
    page_size: u64,
    max_iov: u64,
    target_pid: Pid,
    mem_fd: &mut File,
    mem_regions: &Vec<MemRegion>,
    restore_data: &Vec<u8>,
) {
    let mut count: usize = 0;
    let mut local_iovecs: Vec<uio::IoVec<&[u8]>> = Vec::new();
    let mut remote_iovecs: Vec<uio::RemoteIoVec> = Vec::new();
    while count < i {
        let mut offset: usize = 0;
        for region in mem_regions.iter() {
            for page_num in 0..region.num_pages {
                let page_offset = offset + (page_size * page_num) as usize;
                local_iovecs.push(uio::IoVec::from_slice(
                    &restore_data[page_offset..page_offset + page_size as usize],
                ));
                remote_iovecs.push(uio::RemoteIoVec {
                    base: (region.start + (page_size * page_num)) as usize,
                    len: page_size as usize,
                });

                if local_iovecs.len() as u64 == max_iov {
                    uio::process_vm_writev(target_pid, &local_iovecs, &remote_iovecs)
                        .expect("Could not reset child process");
                    local_iovecs.clear();
                    remote_iovecs.clear();
                }
            }
            if sparse_page_per_region {
                count += 1;
            } else {
                // every page counts
                count += region.num_pages as usize;
            }
            offset += region.size;
        }
    }
    if local_iovecs.len() > 0 {
        uio::process_vm_writev(target_pid, &local_iovecs, &remote_iovecs)
            .expect("Could not reset child process");
    }
}

/// Restore `i` number of pages by looking up the soft-dirty bit of each page
/// in each region. Since the target process won't have any dirty bits (it
/// never fully runs), we'll consider all pages dirty. We still need to
/// look up the value though.
///
/// If `sparse_page_per_region` is true, only the middle page in the memory
/// region will be "found" as dirty, with regions being repeatedly checked for
/// soft-dirty pages until the total count == i.
///
/// If `sparse_page_per_region` is false, every page in the region will be
/// counted towards the total count.
fn restore_via_mem_with_dirty_pages(
    i: usize,
    sparse_page_per_region: bool,
    page_size: u64,
    mem_fd: &mut File,
    pagemap_fd: &mut File,
    clear_refs_fd: &mut File,
    mem_regions: &Vec<MemRegion>,
    restore_data: &Vec<u8>,
) {
    // doesn't need to happen at the end, just needs to happen once
    clear_refs_fd.write(b"4").expect("Could not clear refs");

    let mut count: usize = 0;
    while count < i {
        let mut offset: usize = 0;
        for region in mem_regions.iter() {
            let mut page_data = vec![0; region.num_pages as usize * 8];
            pagemap_fd
                .seek(SeekFrom::Start(
                    region.pagemap_offset + (8 * region.num_pages),
                ))
                .expect("Could not seek");
            pagemap_fd.read_exact(&mut page_data).unwrap();
            let middle_page_num = region.num_pages >> 1;
            for page_num in 0..region.num_pages {
                let dirty: u8 = page_data[(page_num as usize * 8) + 6] & (1 << 7);
                // opaque predicate
                let is_dirty: bool = dirty + 1 > 0;
                let page_offset = offset + (page_size * page_num) as usize;

                if sparse_page_per_region && page_num != middle_page_num {
                    continue;
                }

                mem_fd
                    .seek(SeekFrom::Start(region.start))
                    .expect("Could not seek");
                mem_fd
                    .write(&restore_data[page_offset..page_offset + page_size as usize])
                    .expect("Could not restore memory");
                count += 1;
                if count >= i {
                    return;
                }
            }
            offset += region.size;
        }
    }
}

/// Restore `i` number of pages by looking up the soft-dirty bit of each page
/// in each region. Since the target process won't have any dirty bits (it
/// never fully runs), we'll consider all pages dirty. We still need to
/// look up the value though.
///
/// If `sparse_page_per_region` is true, only the middle page in the memory
/// region will be "found" as dirty, with regions being repeatedly checked for
/// soft-dirty pages until the total count == i.
///
/// If `sparse_page_per_region` is false, every page in the region will be
/// counted towards the total count.
fn restore_via_process_vm_writev_with_dirty_pages(
    i: usize,
    sparse_page_per_region: bool,
    target_pid: Pid,
    page_size: u64,
    mem_fd: &mut File,
    pagemap_fd: &mut File,
    clear_refs_fd: &mut File,
    mem_regions: &Vec<MemRegion>,
    restore_data: &Vec<u8>,
) {
    // doesn't need to happen at the end, just needs to happen once
    clear_refs_fd.write(b"4").expect("Could not clear refs");

    let mut count: usize = 0;
    let mut local_iovecs: Vec<uio::IoVec<&[u8]>> = Vec::new();
    let mut remote_iovecs: Vec<uio::RemoteIoVec> = Vec::new();
    while count < i {
        let mut offset: usize = 0;
        for region in mem_regions.iter() {
            let mut page_data = vec![0; region.num_pages as usize * 8];
            pagemap_fd
                .seek(SeekFrom::Start(
                    region.pagemap_offset + (8 * region.num_pages),
                ))
                .expect("Could not seek");
            pagemap_fd.read_exact(&mut page_data).unwrap();
            let middle_page_num = region.num_pages >> 1;
            for page_num in 0..region.num_pages {
                let dirty: u8 = page_data[(page_num as usize * 8) + 6] & (1 << 7);
                // opaque predicate - have to check all of them!
                let is_dirty: bool = dirty + 1 > 0;
                let page_offset = offset + (page_size * page_num) as usize;

                if sparse_page_per_region && page_num != middle_page_num {
                    continue;
                }

                local_iovecs.push(uio::IoVec::from_slice(
                    &restore_data[page_offset..page_offset + page_size as usize],
                ));
                remote_iovecs.push(uio::RemoteIoVec {
                    base: (region.start + (page_size * page_num)) as usize,
                    len: page_size as usize,
                });
                count += 1;
                if count >= i {
                    return;
                }
            }
            offset += region.size;
        }
    }
    uio::process_vm_writev(target_pid, &local_iovecs, &remote_iovecs)
        .expect("Could not reset child process");
}

pub fn _compare_snapshot_restores(c: &mut Criterion, show_dense_region: bool) {
    let plot_config = PlotConfiguration::default().summary_scale(AxisScale::Logarithmic);

    let target = unsafe {
        Command::new("/bin/bash")
            .args(vec!["-c", "echo hello"])
            .pre_exec(|| {
                ptrace::traceme().expect("Could not trace process");
                Ok(())
            })
            .spawn()
            .expect("Could not spawn")
    };
    let target_pid = Pid::from_raw(target.id() as i32);
    match waitpid(target_pid, None) {
        Ok(WaitStatus::Stopped(_, Signal::SIGTRAP)) => {
            // ready to go now
        }
        _ => panic!("COULD NOT START"),
    }

    let mut mem_fd = std::fs::OpenOptions::new()
        .read(true)
        .write(true)
        .open(&format!("/proc/{}/mem", target_pid))
        .expect("Could not open /proc/pid/mem");
    let mut clear_refs_fd: File = File::create(&format!("/proc/{}/clear_refs", target_pid))
        .expect(&format!("Could not open /proc/{}/clear_refs", target_pid));
    let mut pagemap_fd: File = File::open(&format!("/proc/{}/pagemap", target_pid))
        .expect(&format!("Could not open /proc/{}/pagemap", target_pid));

    let page_size = unsafe { sysconf(_SC_PAGESIZE) } as u64;
    let iov_max = unsafe { sysconf(_SC_IOV_MAX) } as u64;
    let mut mem_regions: Vec<MemRegion> = Vec::new();
    let mut restore_data: Vec<u8> = Vec::new();
    load_mem_regions(
        page_size,
        target_pid,
        &mut mem_regions,
        &mut mem_fd,
        &mut restore_data,
    );

    let mut group = c.benchmark_group(if show_dense_region {
        "snapshot_restore_with_dense"
    } else {
        "snapshot_restore_without_dense"
    });
    group.plot_config(plot_config);
    group.sample_size(100);
    group.measurement_time(std::time::Duration::new(5, 0));

    let increments: [f32; 10] = [0.0, 0.333, 0.666, 1.0, 1.333, 1.666, 2.0, 2.333, 2.666, 3.0];

    // number of pages to restore
    for incr in increments.iter() {
        let i: usize = 10.0f32.powf(*incr).round() as usize;
        group.bench_with_input(
            BenchmarkId::new("Sparse: Entire region + /proc/pid/mem", i),
            &i,
            |b, i| {
                b.iter(|| restore_via_mem(*i, true, &mut mem_fd, &mem_regions, &restore_data));
            },
        );

        group.bench_with_input(
            BenchmarkId::new("Sparse: Entire region + process_vm_writev", i),
            &i,
            |b, i| {
                b.iter(|| {
                    restore_region_via_process_vm_write(
                        *i,
                        true,
                        target_pid,
                        &mut mem_fd,
                        &mem_regions,
                        &restore_data,
                    )
                });
            },
        );

        group.bench_with_input(
            BenchmarkId::new("Dense: Entire region + /proc/pid/mem", i),
            &i,
            |b, i| {
                b.iter(|| restore_via_mem(*i, false, &mut mem_fd, &mem_regions, &restore_data));
            },
        );

        group.bench_with_input(
            BenchmarkId::new("Dense: Entire region + process_vm_writev", i),
            &i,
            |b, i| {
                b.iter(|| {
                    restore_region_via_process_vm_write(
                        *i,
                        false,
                        target_pid,
                        &mut mem_fd,
                        &mem_regions,
                        &restore_data,
                    )
                });
            },
        );

        group.bench_with_input(
            BenchmarkId::new("Sparse: Entire region + process_vm_writev w/ page size", i),
            &i,
            |b, i| {
                b.iter(|| {
                    restore_region_via_process_vm_write_with_page_size(
                        *i,
                        true,
                        page_size,
                        iov_max,
                        target_pid,
                        &mut mem_fd,
                        &mem_regions,
                        &restore_data,
                    )
                });
            },
        );

        group.bench_with_input(
            BenchmarkId::new("Dense: Entire region + process_vm_writev w/ page size", i),
            &i,
            |b, i| {
                b.iter(|| {
                    restore_region_via_process_vm_write_with_page_size(
                        *i,
                        false,
                        page_size,
                        iov_max,
                        target_pid,
                        &mut mem_fd,
                        &mem_regions,
                        &restore_data,
                    )
                });
            },
        );

        group.bench_with_input(
            BenchmarkId::new("Sparse: Dirty pages + /proc/pid/mem", i),
            &i,
            |b, i| {
                b.iter(|| {
                    restore_via_mem_with_dirty_pages(
                        *i,
                        true,
                        page_size,
                        &mut mem_fd,
                        &mut pagemap_fd,
                        &mut clear_refs_fd,
                        &mem_regions,
                        &restore_data,
                    )
                })
            },
        );

        group.bench_with_input(
            BenchmarkId::new("Sparse: Dirty pages + process_vm_writev", i),
            &i,
            |b, i| {
                b.iter(|| {
                    restore_via_process_vm_writev_with_dirty_pages(
                        *i,
                        true,
                        target_pid,
                        page_size,
                        &mut mem_fd,
                        &mut pagemap_fd,
                        &mut clear_refs_fd,
                        &mem_regions,
                        &restore_data,
                    )
                })
            },
        );

        group.bench_with_input(
            BenchmarkId::new("Dense: Dirty pages + /proc/pid/mem", i),
            &i,
            |b, i| {
                b.iter(|| {
                    restore_via_mem_with_dirty_pages(
                        *i,
                        false,
                        page_size,
                        &mut mem_fd,
                        &mut pagemap_fd,
                        &mut clear_refs_fd,
                        &mem_regions,
                        &restore_data,
                    )
                })
            },
        );

        group.bench_with_input(
            BenchmarkId::new("Dense: Dirty pages + process_vm_writev", i),
            &i,
            |b, i| {
                b.iter(|| {
                    restore_via_process_vm_writev_with_dirty_pages(
                        *i,
                        false,
                        target_pid,
                        page_size,
                        &mut mem_fd,
                        &mut pagemap_fd,
                        &mut clear_refs_fd,
                        &mem_regions,
                        &restore_data,
                    )
                })
            },
        );
    }
    group.finish();

    ptrace::kill(target_pid).expect("Could not kill the child pid");
}

pub fn compare_snapshot_restores_with_dense(c: &mut Criterion) {
    _compare_snapshot_restores(c, true);
}

pub fn compare_snapshot_restores_without_dense(c: &mut Criterion) {
    _compare_snapshot_restores(c, false);
}

criterion_group!(benches, compare_snapshot_restores_with_dense,);
criterion_main!(benches);
