extern crate libc;
extern crate nix;
#[macro_use]
extern crate memoffset;

#[macro_use]
mod log;
mod corpus;
mod coverage;
mod debug;
mod mutate;
mod random;
mod snapshot;
mod utils;

use std::env;
use std::time;

use nix::sys::ptrace;
use nix::sys::signal::Signal;
use nix::sys::wait::{waitpid, WaitStatus};
use nix::unistd::Pid;

use crate::corpus::Corpus;
use crate::coverage::{CoverageMonitor, CoverageStats};
use crate::debug::{set_watchpoint, spawn, WatchpointSize};
use crate::mutate::mutate;
use crate::random::Rand;
use crate::snapshot::Snapshot;
use crate::utils::ptrace_get_fpregs;

/// Perform one fuzz iteration, returning a tuple containing `(bool, CoverageStats)`
/// with the boolean set to true if a crash occurred.
fn run_once(
    child_pid: Pid,
    coverage: &mut CoverageMonitor,
    snapshot: &Snapshot,
    overwrite_data_addr: u64,
    fuzz_data: &[u8],
) -> (bool, CoverageStats) {
    snapshot.restore(vec![(overwrite_data_addr as usize, fuzz_data)]);
    coverage.start();
    ptrace::cont(child_pid, None).expect("Could not continue process");

    let res = match waitpid(child_pid, None) {
        Ok(WaitStatus::Stopped(_, Signal::SIGTRAP)) => {
            // hit final breakpoint, simply fall through
            false
        }
        Ok(WaitStatus::Exited(_, status)) => {
            log_debug!("Process exited with status {}", status);
            false
        }
        Ok(WaitStatus::Stopped(_, signal)) => {
            log_success!("CRASH! signal: {}", signal);
            true
        }
        Ok(WaitStatus::Signaled(_, signal, val)) => {
            log_debug!("Signaled: {} val: {}", signal, val);
            false
        }
        Err(_) => {
            log_debug!("Could not waitpid");
            false
        }
        _ => {
            log_debug!("Other event");
            false
        }
    };

    coverage.stop();
    let stats = coverage.stats().unwrap();
    (res, stats)
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        log_debug!("USAGE: ptrace_test target_bin [target args ...]");
        return;
    }

    let mut _overwrite_data_addr: Option<u64> = None;
    let mut _overwrite_data_max_len: Option<usize> = None;
    let mut snapshot: Option<Snapshot> = None;

    let child_pid = spawn(&args[1], &args[2..args.len()]);
    log_debug!("Spawned child process {}", child_pid);

    loop {
        match waitpid(child_pid, None) {
            Ok(WaitStatus::Exited(_, status)) => {
                log_debug!("Process exited with status {}", status);
                break;
            }
            Ok(WaitStatus::Stopped(_, Signal::SIGTRAP)) => {
                log_debug!("Debugee stopped");
                let regs = ptrace::getregs(child_pid).unwrap();

                // First breakpoint, rax should be a pointer to memory that we
                // want to set a breakpoint on
                if regs.rcx == 0xf00dfeed {
                    log_debug!("Received tagged memory address");
                    log_debug!("| bp addr: {:x}", regs.rax);
                    log_debug!("| max_mem_len: {}", regs.rbx);
                    log_debug!("| rip: {:x}", regs.rip);
                    _overwrite_data_addr = Some(regs.rax);
                    _overwrite_data_max_len = Some(regs.rbx as usize);
                    // has to be on a word boundary (can't be an odd number)
                    set_watchpoint(child_pid, regs.rax & !1, WatchpointSize::Two, false);

                // Final breakpoint - should not happen yet! this means our
                // data was never accessed
                } else if regs.rcx == 0xfeedf00d {
                    panic!("Should have hit our watchpoint first");

                // Hit the read breakpoint!
                } else {
                    log_debug!("Watchpoint hit");
                    log_debug!("| rip: {:x}", regs.rip);
                    log_debug!("| rax: {:x}", regs.rax);
                    log_debug!("| rbx: {:x}", regs.rbx);
                    log_debug!("| rcx: {:x}", regs.rcx);
                    log_debug!("| rdx: {:x}", regs.rdx);
                    log_debug!("| rdi: {:x}", regs.rdi);
                    log_debug!("| rsi: {:x}", regs.rsi);

                    // clear the watchpoint before saving the register state
                    set_watchpoint(child_pid, 0, WatchpointSize::Two, true);

                    let fpregs = ptrace_get_fpregs(child_pid).unwrap();
                    let mut tmp = Snapshot::new(child_pid, regs, fpregs);
                    tmp.take();
                    snapshot = Some(tmp);

                    break;
                }
                ptrace::cont(child_pid, None).expect("Should have continued");
            }
            Ok(WaitStatus::Continued(_)) => log_debug!("Continuing"),
            Ok(WaitStatus::PtraceEvent(pid, signal, v)) => {
                log_debug!(
                    "ptrace event: pid: {:?}, signal: {:?}, v: {:?}",
                    pid,
                    signal,
                    v
                );
            }
            Ok(WaitStatus::Signaled(pid, signal, val)) => {
                log_debug!(
                    "ptrace signaled: pid: {:?}, signal: {:?}, val: {:?}",
                    pid,
                    signal,
                    val
                );
            }
            Ok(WaitStatus::StillAlive) => log_debug!("Still alive"),
            Ok(WaitStatus::Stopped(pid, signal)) => {
                log_debug!("Stopped: pid: {:?}, signal: {:?}", pid, signal);
                break;
            }
            Ok(WaitStatus::PtraceSyscall(pid)) => log_debug!("Syscall: {:?}", pid),
            Err(v) => log_debug!("Error!: {:?}", v),
        }
    }

    let mut coverage = match CoverageMonitor::new_for_pid(Some(child_pid.as_raw())) {
        Ok(v) => v,
        Err(v) => {
            log_error!("Could not create CoverageMonitor: {:?}", v);
            log_error!();
            log_error!(
                "Your 'perf_event_paranoid' level is currently '{}' but must be <= 2 in order to use perf events",
                String::from_utf8_lossy(
                    &std::fs::read("/proc/sys/kernel/perf_event_paranoid")
                        .expect("Could not read /proc/sys/kernel/perf_event_paranoid")
                )
                .trim_end()
            );
            log_error!("Set your 'perf_event_paranoid' level to '2' with:");
            log_error!("");
            log_error!("     echo 2 | sudo tee /proc/sys/kernel/perf_event_paranoid");
            log_error!("");
            return;
        }
    };
    let snapshot = snapshot.expect("Snapshot was never taken!");

    log_debug!("Starting main snapshot loop now");

    let start = time::Instant::now();
    let mut iters = 0;
    let overwrite_data_addr = _overwrite_data_addr.unwrap();
    let overwrite_data_max_len = _overwrite_data_max_len.unwrap();
    let mut rand: Rand = Rand::new(2);
    let mut corpus: Corpus = Corpus::new();
    let mut input_data: Vec<u8> = vec![0; overwrite_data_max_len];
    input_data.reserve(overwrite_data_max_len);

    let print_status = |x| {
        let end = time::Instant::now();
        log_info!(
            "{} iters {:.2} iters/s",
            x,
            (x as f64 / (end - start).as_secs_f64()),
        );
    };

    loop {
        iters += 1;
        if iters % 0x7fff == 0 {
            print_status(iters);
        }

        if let Some(v) = corpus.get_item(&mut rand) {
            input_data.resize(v.data.len(), 0);
            input_data.copy_from_slice(&v.data);
        }
        mutate(&mut rand, &mut input_data, overwrite_data_max_len);

        let (crashed, feedback) = run_once(
            child_pid,
            &mut coverage,
            &snapshot,
            overwrite_data_addr,
            &input_data,
        );

        if corpus.has_seen_feedback(feedback) {
            continue;
        }
        log_warn!(
            "New coverage with {:?}",
            String::from_utf8_lossy(&input_data).into_owned()
        );
        corpus.add_item(feedback, &input_data);

        if crashed {
            break;
        }
    }

    let _ = ptrace::kill(child_pid);

    log_debug!("Final stats:");
    print_status(iters);
    log_debug!("{} total iterations", iters);
    log_debug!("{} items in corpus", corpus.num_items());
}
