use hashbrown::HashSet;

use crate::coverage::CoverageStats;
use crate::random::Rand;

pub struct Item {
    pub feedback: CoverageStats,
    pub data: Vec<u8>,
}

pub struct Corpus {
    seen_feedback: HashSet<(u64, u64, u64)>,
    items: Vec<Item>,
}

impl Corpus {
    pub fn new() -> Corpus {
        Corpus {
            seen_feedback: HashSet::new(),
            items: Vec::new(),
        }
    }

    pub fn num_items(&self) -> usize {
        self.items.len()
    }

    pub fn has_seen_feedback(&self, stats: CoverageStats) -> bool {
        self.seen_feedback
            .contains(&(stats.cycles, stats.instrs, stats.branches))
    }

    /// Adds the item to the coverage and returns true if the feedback stats
    /// are unique. Otherwise returns false and does not add it
    pub fn add_item(&mut self, stats: CoverageStats, data: &Vec<u8>) -> bool {
        let key = (stats.cycles, stats.instrs, stats.branches);
        if self.seen_feedback.contains(&key) {
            return false;
        }
        self.seen_feedback.insert(key);
        self.items.push(Item {
            feedback: stats,
            data: data.to_owned(),
        });
        true
    }

    pub fn get_item(&self, rand: &mut Rand) -> Option<&Item> {
        let len = self.items.len();
        if len == 0 {
            return None;
        }

        let rand_idx = if len > 4 {
            let half_size = len / 2;
            let first_half = len - half_size;
            // double the odds of the last half
            let new_size = len + half_size;
            let tmp = rand.next() as usize % new_size;
            if tmp < first_half {
                tmp
            } else {
                first_half + (tmp - first_half) / 2
            }
        } else {
            rand.next() as usize % len
        };
        self.items.get(rand_idx)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_set_features() {
        let mut corpus = Corpus::new();
        let mut rand = Rand::new(100);
        let stats = CoverageStats {
            instrs: 1,
            cycles: 2,
            branches: 3,
        };
        let data: Vec<u8> = vec![0, 1, 2, 3];
        assert_eq!(corpus.add_item(stats, &data), true);
        assert_eq!(corpus.add_item(stats, &data), false);

        let rand_item = corpus.get_item(&mut rand).unwrap();
        assert_eq!(rand_item.data, data);
    }
}
