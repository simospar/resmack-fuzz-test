use crate::random::Rand;

pub fn mutate(rand: &mut Rand, data: &mut Vec<u8>, max_len: usize) {
    if data.len() == 0 {
        mut_insert_rand(rand, data, max_len);
        return;
    }
    let idx = rand.next();
    match idx % 2 {
        0 => mut_insert_rand(rand, data, max_len),
        1 => mut_change_rand(rand, data),
        _ => panic!("Math is broken"),
    };
}

#[inline]
fn mut_insert_rand(rand: &mut Rand, data: &mut Vec<u8>, max_len: usize) {
    let orig_len = data.len();
    // 5 bytes to choose random indices
    // 1 bytes to num bytes to insert % 5
    // 2 bytes to choose insert index
    let rand_num: u64 = rand.next();
    let rand_num_bytes: usize = (rand_num & 0xff) as usize;
    let rand_byte_data = (rand_num >> 8) & 0xffffffffff;
    let rand_offset = ((rand_num >> 48) & 0xffff) as usize;

    let rand_idx = if orig_len == 0 {
        0
    // needs to be inside the current data
    } else if orig_len == max_len {
        rand_offset % orig_len
    // inside or append
    } else {
        rand_offset % (orig_len + 1)
    };

    // always at least one, but make sure it fits in the data
    let mut num_bytes = (rand_num_bytes % 4) + 1;
    if num_bytes > (max_len - rand_idx) {
        num_bytes = max_len - rand_idx;
    }

    let new_len = std::cmp::min(max_len, orig_len + num_bytes);

    let mut new_data: [u8; 5] = [0; 5];
    for x in 0..num_bytes {
        new_data[x as usize] = ((rand_byte_data >> (8 * x)) & 0xff) as u8;
    }
    if orig_len != new_len {
        data.resize(new_len, 0);
    }

    if orig_len > 0 && rand_idx < orig_len {
        let num_to_shift = std::cmp::min(num_bytes, new_len - rand_idx - num_bytes);
        data.copy_within(rand_idx..rand_idx + num_to_shift, rand_idx + num_bytes);
    }
    data[rand_idx..(rand_idx + num_bytes)].copy_from_slice(&new_data[0..num_bytes]);
}

#[inline]
fn mut_change_rand(rand: &mut Rand, data: &mut [u8]) {
    let rand_num: u64 = rand.next();
    let rand_offset: usize = ((rand_num & 0xff) as usize) % data.len();
    let rand_val = ((rand_num >> 8) & 0xff) as u8;
    data[rand_offset] = rand_val;
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_insert_mutation() {
        let mut rand: Rand = Rand::new(101);
        let mut data: Vec<u8> = Vec::new();

        for _ in 0..10 {
            mut_insert_rand(&mut rand, &mut data, 10);
        }

        assert_eq!(data.len(), 10);
        assert_ne!(data, [0u8; 10]);
    }

    #[test]
    fn test_change_mutation() {
        let mut rand: Rand = Rand::new(200);
        let mut data: [u8; 10] = [0; 10];
        mut_change_rand(&mut rand, &mut data);
        assert_ne!(data, [0u8; 10]);
    }
}
