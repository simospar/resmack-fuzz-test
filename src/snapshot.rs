extern crate libc;
extern crate nix;

use std::fmt;
use std::fs::File;
use std::io::{Read, Seek, SeekFrom, Write};

use libc::{sysconf, _SC_PAGESIZE};

use nix::sys::ptrace;
use nix::sys::uio;
use nix::unistd::Pid;

use crate::utils::ptrace_set_fpregs;

pub struct MemRegion {
    pub idx: u64,
    pub start: u64,
    pub end: u64,
    pub size: usize,
    pub pagemap_offset: u64,
    pub num_pages: u64,
}

impl std::fmt::Debug for MemRegion {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Page[{}]<{:x}-{:x}>", self.idx, self.start, self.end)
    }
}

impl MemRegion {
    pub fn new(page_size: u64, idx: u64, start: u64, end: u64) -> MemRegion {
        let num_pages = (end - start) / page_size;
        let pagemap_offset = (start / page_size) * 8;
        let size = end - start;

        MemRegion {
            idx,
            start,
            end,
            size: size as usize,
            pagemap_offset: pagemap_offset,
            num_pages: num_pages,
        }
    }
}

pub struct Snapshot {
    rw_regions: Vec<MemRegion>,
    data: Vec<u8>,
    regs: libc::user_regs_struct,
    fpregs: libc::user_fpregs_struct,
    pid: Pid,
    page_size: u64,
    clear_refs_fd: File,
    pagemap_fd: File,
}

impl Snapshot {
    pub fn new(
        pid: Pid,
        regs: libc::user_regs_struct,
        fpregs: libc::user_fpregs_struct,
    ) -> Snapshot {
        log_debug!("New snapshot with rip at {:x}", regs.rip);
        Snapshot {
            rw_regions: Vec::new(),
            data: Vec::new(),
            regs,
            fpregs,
            pid,
            page_size: unsafe { sysconf(_SC_PAGESIZE) } as u64,
            clear_refs_fd: File::create(&format!("/proc/{}/clear_refs", pid))
                .expect(&format!("Could not open /proc/{}/clear_refs", pid)),
            pagemap_fd: File::open(&format!("/proc/{}/pagemap", pid))
                .expect(&format!("Could not open /proc/{}/pagemap", pid)),
        }
    }

    #[allow(dead_code)]
    pub fn print_regs(&self) {
        let regs = self.regs;
        log_debug!("| rip: {:x}", regs.rip);
        log_debug!("| rax: {:x}", regs.rax);
        log_debug!("| rbx: {:x}", regs.rbx);
        log_debug!("| rcx: {:x}", regs.rcx);
        log_debug!("| rdx: {:x}", regs.rdx);
        log_debug!("| rdi: {:x}", regs.rdi);
        log_debug!("| rsi: {:x}", regs.rsi);
    }

    pub fn take(&mut self) {
        log_info!("Taking snapshot");
        self.load_rw_page_info();
        self.data.clear();

        let mut mem_fd: File = std::fs::File::open(&format!("/proc/{}/mem", self.pid))
            .expect(&format!("Could not open /proc/{}/mem", self.pid));

        for page in self.rw_regions.iter() {
            let range_total = page.end - page.start;
            let mut range_data = vec![0; range_total as usize];
            mem_fd
                .seek(SeekFrom::Start(page.start))
                .expect("Could not seek");
            mem_fd.read_exact(&mut range_data).expect("could not read");
            self.data.extend(range_data);
        }
        log_debug!(
            "Copied {} regions | {} pages | {} bytes for snapshot",
            self.rw_regions.len(),
            self.page_size,
            self.data.len()
        );

        self.clear_refs();
    }

    fn load_rw_page_info(&mut self) {
        self.rw_regions.clear();

        let output = std::fs::read_to_string(&format!("/proc/{}/maps", self.pid)).unwrap();
        for (idx, line) in output.split("\n").enumerate() {
            //            0                1      2       3     4                             5
            // 558e65308000-558e6530a000 r--p 00000000 fd:01 36700321 /usr/bin/cat
            // 558e65308000-558e6530a000 r--p 00000000 fd:01 36700321 [stack]
            let parts: Vec<&str> = line.split_whitespace().collect();
            let perms = match parts.get(1) {
                Some(v) => v.as_bytes(),
                None => continue,
            };
            // rwxp
            if perms[1] != b'w' {
                continue;
            }

            let range_parts: Vec<&str> = parts[0].split("-").collect();
            let start: u64 =
                u64::from_str_radix(range_parts[0], 16).expect("Could not parse start range");
            let end: u64 =
                u64::from_str_radix(range_parts[1], 16).expect("Could not parse end range");
            self.rw_regions
                .push(MemRegion::new(self.page_size, idx as u64, start, end));
        }
    }

    /// Restore the snapshot, optionally writing additional data into memory
    /// after restoring the program state.
    pub fn restore(&self, addtl: Vec<(usize, &[u8])>) {
        let mut local_iovecs: Vec<uio::IoVec<&[u8]>> = Vec::new();
        let mut remote_iovecs: Vec<uio::RemoteIoVec> = Vec::new();
        let mut offset: usize = 0;

        for region in self.rw_regions.iter() {
            let mut page_data = vec![0; region.num_pages as usize * 8];
            (&self.pagemap_fd)
                .seek(SeekFrom::Start(region.pagemap_offset))
                .expect("Could not seek");
            (&self.pagemap_fd).read_exact(&mut page_data).unwrap();

            for page_num in 0..region.num_pages {
                let dirty: u8 = page_data[(page_num as usize * 8) + 6] & (1 << 7);
                if dirty == 0 {
                    continue;
                }
                let page_offset = offset + (self.page_size * page_num) as usize;
                local_iovecs.push(uio::IoVec::from_slice(
                    &self.data[page_offset..page_offset + self.page_size as usize],
                ));
                remote_iovecs.push(uio::RemoteIoVec {
                    base: region.start as usize,
                    len: self.page_size as usize,
                });
            }
            offset += region.size;
        }

        for (addr, data) in addtl.iter() {
            local_iovecs.push(uio::IoVec::from_slice(data));
            remote_iovecs.push(uio::RemoteIoVec {
                base: *addr,
                len: data.len(),
            });
        }

        if local_iovecs.len() > 0 {
            uio::process_vm_writev(self.pid, &local_iovecs, &remote_iovecs)
                .expect("Could not reset child process");
        }

        ptrace::setregs(self.pid, self.regs).expect("Could not restore registers");
        ptrace_set_fpregs(self.pid, self.fpregs).expect("Could not restore fp registers");

        self.clear_refs();
    }

    /// Reset the soft-dirty bit on pages. This is used to tell if a page has
    /// been written to since we last wrote "4" to /proc/pid/clear_refs. To
    /// quote the man page `procfs(5)`:
    ///
    /// > 4 (since Linux 3.11)
    /// >
    /// >     Clear the soft-dirty bit for all the pages associated
    /// >     with the process.  This is used (in conjunction with
    /// >     /proc/[pid]/pagemap) by the check-point restore system
    /// >     to discover which pages of a process have been dirtied
    /// >     since the file /proc/[pid]/clear_refs was written to.
    ///
    fn clear_refs(&self) {
        // reset soft dirty flags
        (&self.clear_refs_fd).write(b"4").unwrap();
    }
}
