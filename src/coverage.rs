extern crate perfcnt;

use perfcnt::linux::{HardwareEventType, PerfCounterBuilderLinux};
use perfcnt::{AbstractPerfCounter, PerfCounter};

#[derive(Debug, Copy)]
pub struct CoverageStats {
    pub cycles: u64,
    pub instrs: u64,
    pub branches: u64,
}

impl Clone for CoverageStats {
    fn clone(&self) -> Self {
        CoverageStats {
            cycles: self.cycles,
            instrs: self.instrs,
            branches: self.branches,
        }
    }
}

pub struct CoverageMonitor {
    #[allow(dead_code)]
    pid: libc::pid_t,
    branches: PerfCounter,
    //cycles: PerfCounter,
    instrs: PerfCounter,
}

/// This uses the perf-event (PAPI) counters to measure instruction, cpu cycles,
/// and branch instructions taken.
///
/// If you see *permission denied* errors, you may need to set
/// `/proc/sys/kernel/perf_event_paranoid` to at least `2` or less:
///
/// ```bash
/// echo 2 | sudo tee /proc/sys/kernel/perf_event_paranoid
/// ```
///
/// Unsure, but may also need linux-tools-generic installed
impl CoverageMonitor {
    pub fn new_for_pid(pid: Option<libc::pid_t>) -> Result<CoverageMonitor, std::io::Error> {
        let pid = match pid {
            Some(v) => v,
            None => 0,
        };

        Ok(CoverageMonitor {
            pid: pid,
            branches: PerfCounterBuilderLinux::from_hardware_event(
                HardwareEventType::BranchInstructions,
            )
            .for_pid(pid)
            .inherit()
            .disable()
            .exclude_kernel()
            .exclude_hv()
            .exclude_idle()
            .finish()?,
            /*
            cycles: PerfCounterBuilderLinux::from_hardware_event(HardwareEventType::CPUCycles)
                .for_pid(pid)
                .inherit()
                .disable()
                .exclude_kernel()
                .exclude_hv()
                .exclude_idle()
                .finish()?,
            */
            instrs: PerfCounterBuilderLinux::from_hardware_event(HardwareEventType::Instructions)
                .for_pid(pid)
                .inherit()
                .disable()
                .exclude_kernel()
                .exclude_hv()
                .exclude_idle()
                .finish()?,
        })
    }

    pub fn start(&mut self) {
        self.branches.reset().unwrap();
        self.instrs.reset().unwrap();
        //self.cycles.reset().unwrap();

        self.branches.start().unwrap();
        self.instrs.start().unwrap();
        //self.cycles.start().unwrap();
    }

    pub fn stop(&mut self) {
        self.branches.stop().unwrap();
        self.instrs.stop().unwrap();
        //self.cycles.stop().unwrap();
    }

    pub fn stats(&mut self) -> Option<CoverageStats> {
        Some(CoverageStats {
            cycles: 0,
            /*cycles: self
            .cycles
            .read()
            .expect("Could not read the cycles counter"),*/
            instrs: self
                .instrs
                .read()
                .expect("Could not read the instrs counter"),
            branches: self
                .branches
                .read()
                .expect("Could not read the branches counter"),
        })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_coverage() {
        // monitor this pid
        let mut cov = CoverageMonitor::new_for_pid(None).unwrap();
        cov.start();
        println!("hello");
        cov.stop();
        let stats = cov.stats().unwrap();
        assert_eq!(stats.instrs > 0, true);
        assert_eq!(stats.branches > 0, true);
    }
}
