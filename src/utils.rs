extern crate nix;

use libc::{self, c_void};
use nix::errno::Errno;
use nix::sys::ptrace::{Request, RequestType};
use nix::unistd::Pid;
use nix::Result;
use std::{mem, ptr};

pub fn ptrace_get_fpregs(pid: Pid) -> Result<libc::user_fpregs_struct> {
    ptrace_get_data::<libc::user_fpregs_struct>(Request::PTRACE_GETFPREGS, pid)
}

pub fn ptrace_set_fpregs(pid: Pid, regs: libc::user_fpregs_struct) -> Result<()> {
    let res = unsafe {
        libc::ptrace(
            Request::PTRACE_SETFPREGS as RequestType,
            libc::pid_t::from(pid),
            ptr::null_mut::<c_void>(),
            &regs as *const _ as *const c_void,
        )
    };
    Errno::result(res).map(drop)
}

fn ptrace_get_data<T>(request: Request, pid: Pid) -> Result<T> {
    let mut data = mem::MaybeUninit::uninit();
    let res = unsafe {
        libc::ptrace(
            request as RequestType,
            libc::pid_t::from(pid),
            ptr::null_mut::<T>(),
            data.as_mut_ptr() as *const _ as *const c_void,
        )
    };
    Errno::result(res)?;
    Ok(unsafe { data.assume_init() })
}
